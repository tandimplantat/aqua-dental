# Aqua Dental #
![tandimplantatsspecialisterna-logo.jpg](https://bitbucket.org/repo/qEzgR77/images/3674903888-tandimplantatsspecialisterna-logo.jpg)

**ABOUT US** 

Som en del av tandläkarkedjan **[Aqua Dental](http://tandimplantatspecialisterna.se/)** är vi experter på tandimplantat. Hos oss får du tandvård med kvalitet till rätt pris.






**Phone:** 020 20 20 80

**Email:** social@aquadental.se

**Address:** Stureplan 4a, 114 35 Stockholm, Sweden